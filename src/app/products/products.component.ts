import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;
//deleteProduct(product){
  //this._productsService.deleteProduct(product);
//}
updateProduct(product){
this._productsService.updateProduct(product);
}
deleteProduct(product){
this._productsService.removeProducts(product);
}
  constructor(private _productsService: ProductsService) { }

  ngOnInit() {
        this._productsService.getproducts().subscribe(postData => {this.products = postData;console.log(this.products)});

  }

}
