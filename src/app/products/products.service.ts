import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {AngularFire} from 'angularfire2'; 


@Injectable()
export class ProductsService {

  productObservable;

removeProducts(product){
  this.productObservable.remove(product);
}
updateProduct(product){
let productTemp = {name:product.name,cost:product.cost}
console.log(productTemp);
this.af.database.object('/product/' + product.$key).update(productTemp);

}

getProductsa(){
      this.productObservable = this.af.database.list('product');
      return this.productObservable;

}

  constructor(private af:AngularFire) { }


  getproducts(){
    this.productObservable = this.af.database.list('/product').map(
      products =>{
        products.map(
          product => {
            product.catName = [];
            for(var p in product.category){
                product.catName.push(
                this.af.database.object('/category/' + p)
              )
            }
          }
        );
        return products;
      }
    )
    //this.productObservable = this.af.database.list('/users');
    return this.productObservable;
	}
}
