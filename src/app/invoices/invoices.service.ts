import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
@Injectable()
export class InvoicesService {

invoiceObservable;

addInvoice(invoice){
      this.invoiceObservable = this.af.database.list('/invoices');
    this.invoiceObservable.push(invoice);
  }
  
getInvoices(){
      this.invoiceObservable = this.af.database.list('invoices');
      return this.invoiceObservable;

}



  constructor(private af:AngularFire) { }

}
