import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
 styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

  constructor(private _invoicesService: InvoicesService) { }
onInvoice;
  isLoading = true;
  invoices;
  addInvoice(invoice){
    this.addInvoice(invoice);
  }
 select(invoice){
		this.onInvoice = invoice; 
 }

  ngOnInit() {
        setTimeout(() => 
{
  var foo;
},
5000);   

        this._invoicesService.getInvoices().subscribe(userData => {this.invoices = userData;this.isLoading = false;console.log(this.invoices)});

  }

}
