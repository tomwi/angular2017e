import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoice/invoice'
import {NgForm} from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';


@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {

 //ß @Output() InvoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {
    name: '',
    amount: ''
  };
  //usr:User = {name:'',email:''};  
  constructor(private _InvoiceService: InvoicesService) {
}
  onSubmit(form:NgForm){
    console.log(form);
    this._InvoiceService.addInvoice(this.invoice);
    //this.InvoiceAddedEvent.emit(this.Invoice);
    this.invoice = {
       name: '',
       amount: ''
    }
  }


  ngOnInit() {
  }


}