import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductComponent } from './product/product.component';

import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {FireComponent} from './fire/fire.component';
import{AngularFireModule} from 'angularfire2';
import { ProductsService } from './products/products.service';
import { ProductsComponent } from './products/products.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesService } from './invoices/invoices.service';

import { UsersService } from './users/users.service';

  var config = {
   apiKey: "AIzaSyBL240hjddLykuOwPSwKTbFcJvpavwIg-w",
    authDomain: "pre-test-286bc.firebaseapp.com",
    databaseURL: "https://pre-test-286bc.firebaseio.com",
    storageBucket: "pre-test-286bc.appspot.com",
    messagingSenderId: "17974141320"
  };

const appRoutes: Routes = [
  // { path: 'users', component: UsersComponent },
  // { path: 'posts', component: PostsComponent },
  //   { path: 'products', component: ProductsComponent },

  //  { path: 'fire', component: FireComponent },
    { path: 'invoiceform', component: InvoiceFormComponent },
      { path: 'invoices', component: InvoicesComponent },
  { path: '', component: InvoiceFormComponent },

  { path: '**', component: PageNotFoundComponent },
  // { path: '', component: InvoiceFormComponent },

  // { path: '**', component: PageNotFoundComponent },

];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserFormComponent,
    PostsComponent,
    ProductComponent,
    ProductsComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent, 
    FireComponent ,
    InvoiceComponent,
    InvoicesComponent,
    InvoiceFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(config)
  ],
  providers: [UsersService, ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
